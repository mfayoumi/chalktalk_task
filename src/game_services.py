"""
This class contains the game rules, utils and score calculation
"""

class LetterGame():

    def __init__(self, dictionary):
        self.dictionary = dictionary

    ''' Getting the item position/s '''
    def get_item_index(self, item, matrix):
        result = []
        for index, row in enumerate(matrix):
            for k,v in enumerate(row):
                if v == item:
                    result.append((index, k))
        return result

    ''' :returns True if the tiles are neighbour '''
    def check_neighbour(self, first_tile, second_tile, mat):
        item = first_tile
        item_positions = self.get_item_index(item, mat)
        if item_positions is not None:
            for position in item_positions:
                a, b = position
                neighbors = [mat[i][j] for i in range(a - 1, a + 2) for j in range(b - 1, b + 2) if
                             i > -1 and j > -1 and j < len(mat[0]) and i < len(mat)]
                if second_tile in neighbors:
                    return True
        return False

    ''' :returns the word score  '''
    def get_score(self, word):
        return pow(2, len(word) - 3)

    ''' validate a play and returning score or None for invalid word '''
    def play(self, word, mat):
        used_postions = []
        word_list = list(word)
        for index, elem in enumerate(word_list):
            elem_positions = self.get_item_index(elem, mat)
            if not elem_positions:
                return None
            if len(elem_positions) == 1:
                if elem_positions[0] in used_postions:
                    return None
                used_postions.append(elem_positions[0])
            elif len(elem_positions) > 1:
                for position in elem_positions:
                    if position not in used_postions:
                        used_postions.append(position)
                        break
            curr_tile = str(elem)
            if (index + 1 < len(word_list)):
                next_tile = str(word_list[index + 1])
                valid_items = self.check_neighbour(curr_tile, next_tile, mat)
                if not valid_items:
                    return None
        return self.get_score(word)
