from fastapi import FastAPI, Depends, HTTPException, Request
import services
import schemas
import sqlalchemy.orm as orm
from game_services import LetterGame

tags_metadata = [
    {
        "name": "games",
        "description": "Starting a new game with a specific board distribution.",
    },
    {
        "name": "dictionary",
        "description": "Defining the app's dictionary."
    },
    {
        "name": "validate",
        "description": "Validating a play on one of the games previously started",
    }
]

app = FastAPI(title='Letter Game', openapi_tags=tags_metadata)

services.create_database()
games = services.load_games()
dictionary_words = services.load_dictionary_words()

def reload_games():
    global games
    games = services.load_games()

def reload_dictionary_words():
    global dictionary_words
    dictionary_words = services.load_dictionary_words()

@app.post("/v1/games", response_model=schemas.Game, status_code=201, tags=["games"])
def create_game(game: schemas.GameCreate, db: orm.Session=Depends(services.get_db)):
    if not services.is_valid_board(game.board):
        raise HTTPException(status_code=400, detail="Wrong board! It should be 16 alphabetical characters",
                                    headers={"invalid-input": "Wrong board"})

    game.board = game.board.upper()
    result = services.create_game(db=db, game=game)
    reload_games()
    return result

@app.get("/v1/games/{game_id}/validate/{word}", status_code=201, tags=["validate"])
def validate(word: str, game_id: int, request: Request):
    services.get_games()
    if dictionary_words is None or dictionary_words['words'] is None:
        raise HTTPException(status_code=400, detail="No dictionary provided",
                                    headers={"crate-dictionary": request.client.host+"/v1/games/dictionary/"})

    if not services.is_valid_word(word.upper(), dictionary_words):
        raise HTTPException(status_code=422, detail="Word should be at least 3 alphabetical characters",
                            headers={"invalid-input": "Unprocessable word"})

    if word not in dictionary_words['words']:
        raise HTTPException(status_code=404, detail="Word not listed in the dictionary",
                            headers={"not-found": "Not found word"})

    letterGame = LetterGame(dictionary_words)
    score = letterGame.play(word.upper(), games[game_id])
    if score is None:
        return {"score": f"This word cannot be formed in game with ID {game_id}"}
    return {"score": score}

@app.post("/v1/games/dictionary", status_code=201, tags=["dictionary"])
def create_game(items: schemas.DictionaryItems):
    try:
        services.dump_words_dictionary(dict(items))
    except Exception as e:
        raise HTTPException(status_code=422, detail="Invalid input! it should be a dictionary of words e.g. {'word': [...]}",
                                    headers={"invalid-input": "Wrong dictionary format"})
    reload_dictionary_words()
    return {"result": "Dictionary saved"}
