"""
Game table model

This table only includes the game id and board
"""

import sqlalchemy as sql
import database

class Game(database.Base):

    __tablename__ = "game"
    id = sql.Column(sql.Integer, primary_key=True, index=True)
    board = sql.Column(sql.String, index=False)
