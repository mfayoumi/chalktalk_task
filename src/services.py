"""
This is a utils file contains a list of methods for (in memory) data loading, dumping and validation
"""

import database
import schemas
import models

import sqlalchemy.orm as orm
import json

def create_database():
    return database.Base.metadata.create_all(bind=database.engine)

def get_db():
    db = database.SessionLocal()
    try:
        return db
    finally:
        db.close()

def is_valid_board(board: str):
    if len(board) != 16 or not board.isalpha():
        return False
    return True

def is_valid_word(word: str, dictionary: dict):
    if len(word) < 3 or not word.isalpha():
        return False
    return True

def create_game(db: orm.Session, game: schemas.GameCreate):
    db_game = models.Game(board=game.board)
    db.add(db_game)
    db.commit()
    db.refresh(db_game)
    return db_game

def split_list(lst, n):
    for i in range(0, len(lst), n):
        yield lst[i:i + n]

def get_games():
    query = get_db().query(models.Game).all()
    result = {}
    for game in query:
        board = list(game.__dict__['board'])
        board_matrix = list(split_list(board, 4))
        result[game.__dict__['id']] = board_matrix
    return result

def get_dictionary_words():
    try:
        with open('dictionary.json') as json_file:
            data = json.load(json_file)
        return data
    except:
        return None

def load_games():
    return get_games()

def load_dictionary_words():
    return get_dictionary_words()

def dump_words_dictionary(dictionary):
    dictionary['words'] = [word.upper() for word in dictionary['words']]
    with open('dictionary.json', 'w') as json_file:
        json.dump(dictionary, json_file)
