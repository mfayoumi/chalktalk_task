import pydantic as _pydantic

class _GameBase(_pydantic.BaseModel):
    board: str

class GameCreate(_GameBase):
    pass

class Game(_GameBase):
    id: int
    board: str

    class Config:
        orm_mode = True

class DictionaryItems(_pydantic.BaseModel):
    words: list = []
