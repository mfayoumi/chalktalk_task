# ChalkTalk Task: Letter Game
## Overview
This project is a [ChalkTalk's Backend Interview task](https://github.com/ChalkTalk/backend-interview) 
## Install Dependency
``pip install -r requirements.txt``
### Notes
* This project made with [FastAPI](https://fastapi.tiangolo.com/) framework
* SQLite used for this project as DBMS
* For single instance usage: the local memory used as cache memory e.g. `reload_games()` and `reload_dictionary_words()`; see `main.py`
* The APIs documented using swagger and OpenAPI documentations
* `test.py` for Testing strategy description and assertions 
## Run in local development mode
inside `/src` directory run
``uvicorn main:app --reload``

* Local development mode will start the service at port 8000, listen to file changes and reload when needed
* For swagger documentation ```localhost:8000/docs```
* For OpenAPI specification ```localhost:8000/redoc```
## Run in production environment
It is recommended to use gunicorn as per https://www.uvicorn.org/deployment/

* mkdir /opt/letter_game
* copy *.py, requir`ements.txt and models folder to /opt/letter_game
* sudo pip install -r requirements.txt
* sudo pip install gunicorn
* To test if all is okay ``gunicorn -b 127.0.0.1:8000 -w 4 -k uvicorn.workers.UvicornWorker main:app``
* chown [owner]:www-data /opt/letter_game -R
## letter_game.service - Systemd
```
#Systemd unit file for letter-game service
[Unit]
Description=letter-game service
After=network.target

[Service]
Type=notify
RuntimeDirectory=gunicorn
WorkingDirectory=/opt/letter_game
StandardOutput=null

User=[user]
Group=www-data

ExecStart=gunicorn -b 127.0.0.1:8000 -w 4 -k uvicorn.workers.UvicornWorker main:app
ExecReload=/bin/kill -s HUP $MAINPID
KillMode=mixed
PrivateTmp=true


Restart=always

RestartSec=5

OOMScoreAdjust=-500

TimeoutStopSec=180

LimitNOFILE=1048576

[Install]

WantedBy=multi-user.target
```
