from game_services import LetterGame
from services import split_list

"""
L I S T
O F A T
S T R S
O R A Y
"""

"""
Testing Strategy:

1- Defining a static board and static dictionary words.
2- Test a list of words on the defined board and dictionary words.
3- Reverse the word letters and it should gives the same result.
3- Reverse the matrix and test the normal and reversed words. 

"""

if __name__ == '__main__':
    board = list('LISTOFATSTRSORAY')
    board_reverse = board.copy()
    board_reverse.reverse()

    dictionary_words = {"words": ["ARRAY", "ARRAYS", "ART", "ARTS", "FAB", "FAST", "FAT", "FIST", "LIFT"]}
    board_matrix = list(split_list(board, 4))
    board_matrix_reverse = list(split_list(board_reverse, 4))
    words = ["ARRAYS", "ART", "ARTS", "FAB", "FAST", "SEATLE"]
    letterGame = LetterGame(dictionary_words)

    for word in words:
        word_score = letterGame.play(word.upper(), board_matrix)
        reversed_word_score = letterGame.play(word.upper()[::-1], board_matrix)
        if word_score is not None:
            assert word_score == letterGame.get_score(word)
        if reversed_word_score is not None:
            assert reversed_word_score == letterGame.get_score(word)

        word_score = letterGame.play(word.upper(), board_matrix_reverse)
        reversed_word_score = letterGame.play(word.upper()[::-1], board_matrix_reverse)
        if word_score is not None:
            assert word_score == letterGame.get_score(word)
        if reversed_word_score is not None:
            assert reversed_word_score == letterGame.get_score(word)
